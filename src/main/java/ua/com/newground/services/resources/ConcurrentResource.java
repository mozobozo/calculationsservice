package ua.com.newground.services.resources;

import java.lang.ref.WeakReference;
import java.util.WeakHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Resource wrapper that manages read-write locks for each value of provided resource.
 * Each value does not depend on others.
 *
 * Thread safe.
 */
public class ConcurrentResource<T> implements IndexedResource<T> {

    private final WeakHashMap<Integer, WeakReference<ReadWriteLock>> locks = new WeakHashMap<Integer, WeakReference<ReadWriteLock>>();
    private final IndexedResource<T> indexedResource;

    public ConcurrentResource(IndexedResource<T> indexedResource) {
        this.indexedResource = indexedResource;
    }

    @Override
    public T get(int id) {
        Lock readLock = getValueMonitor(id).readLock();

        try{
            readLock.lock();
            return indexedResource.get(id);
        } finally {
            readLock.unlock();
        }
    }

    @Override
    public void put(int id, T value) {
        Lock writeLock = getValueMonitor(id).writeLock();

        try {
            writeLock.lock();
            indexedResource.put(id, value);
        } finally {
            writeLock.unlock();
        }
    }

    /**
     * Returns read-write lock for value with given id.
     */
    private synchronized ReadWriteLock getValueMonitor(int id) {
        WeakReference<ReadWriteLock> lockRef = locks.get(id);
        ReadWriteLock lock = isWeakReferenceEmpty(lockRef) ? null : lockRef.get();

        if (lock == null) {
            lock = new ReentrantReadWriteLock();
            lockRef = new WeakReference(lock);
            locks.put(id, lockRef);
        }

        return lock;
    }

    private boolean isWeakReferenceEmpty(WeakReference ref) {
        return ref == null || ref.get() == null;
    }
}
