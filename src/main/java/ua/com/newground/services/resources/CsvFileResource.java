package ua.com.newground.services.resources;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.math.BigDecimal;

import static au.com.bytecode.opencsv.CSVWriter.DEFAULT_SEPARATOR;
import static au.com.bytecode.opencsv.CSVWriter.NO_QUOTE_CHARACTER;
import static java.lang.Math.max;
import static java.util.Arrays.copyOf;
import static org.apache.commons.io.IOUtils.closeQuietly;
import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * Provides access to decimal values in csv file.
 * Values are kept in memory for faster reads. File is updated on each value update.
 *
 * Not thread safe.
 */
public class CsvFileResource implements IndexedResource<BigDecimal> {
    private static Logger logger = LoggerFactory.getLogger(CsvFileResource.class);

    private final String pathToFile;
    private BigDecimal[] currentValues;

    public CsvFileResource(String pathToFile) throws IOException {
        logger.info("reading csv file resource from {}", pathToFile);
        this.pathToFile = pathToFile;

        File file = new File(pathToFile);
        if (!file.exists()) {
            logger.info("file does not exist - creating empty file");
            file.createNewFile();
            currentValues = new BigDecimal[0];
            return;
        }

        CSVReader csvReader = new CSVReader(new BufferedReader(new FileReader(file)));
        String[] stringValues = csvReader.readNext();
        csvReader.close();

        currentValues = new BigDecimal[stringValues.length];

        for (int i = 0; i < stringValues.length; i++) {
            if (isNotBlank(stringValues[i])) {
                currentValues[i] = new BigDecimal(stringValues[i]);
            }
        }
    }

    @Override
    public BigDecimal get(int id) {
        if (id < 0 || id >= currentValues.length) {
            throw new IllegalArgumentException("Trying to read non-existing value: " + id + " (not in range 0 - " + currentValues.length + ")");
        }
        if (currentValues[id] == null) {
            throw new IllegalArgumentException("Trying to read non-existing value: " + id);
        }

        return currentValues[id];
    }

    @Override
    public void put(int id, BigDecimal value) {
        if (id < 0) {
            throw new IllegalArgumentException("Only positive indexes allowed but was " + id);
        }

        ensureCapacity(id);

        BigDecimal valueBackup = currentValues[id];
        currentValues[id] = value;

        try {
            writeValuesToFile(currentValues, pathToFile);
        } catch (Throwable t) {
            currentValues[id] = valueBackup;
            throw new RuntimeException("Error during csv file write", t);
        }
    }

    private void ensureCapacity(int minimalCapacity) {
        if (minimalCapacity >= currentValues.length) {
            int newLength = (currentValues.length * 3) / 2;
            logger.trace("growing values array to {}", newLength);
            currentValues = copyOf(currentValues, max(minimalCapacity, newLength) + 1);
        }
    }

    private static void writeValuesToFile(BigDecimal[] currentValues, String pathToFile) throws IOException {
        String[] valuesToWrite = new String[currentValues.length];

        for (int i = 0; i < currentValues.length; i++) {
            BigDecimal val = currentValues[i];

            if (val != null) {
                valuesToWrite[i] = val.toString();
            }
        }

        CSVWriter csvWriter = new CSVWriter(new FileWriter(pathToFile), DEFAULT_SEPARATOR, NO_QUOTE_CHARACTER);
        try {
            csvWriter.writeNext(valuesToWrite);
        } finally {
            csvWriter.close();
        }
    }

}
