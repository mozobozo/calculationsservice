package ua.com.newground.services.resources;

/**
 * Represents a resource that contains values accessible by id
 * @param <T> type of resource values
 */
public interface IndexedResource<T> {

    T get(int id);

    void put(int id, T value);

}
