package ua.com.newground.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.com.newground.services.resources.CsvFileResource;
import ua.com.newground.services.responses.GetValueResponse;
import ua.com.newground.services.responses.PostValueResponse;

import javax.ws.rs.*;
import java.math.BigDecimal;


@Path("/calculations")
public class CalculationsService {
    private static Logger logger = LoggerFactory.getLogger(CsvFileResource.class);

    private final ResourcesHandler resourcesHandler;

    public CalculationsService(ResourcesHandler resourcesHandler) {
        this.resourcesHandler = resourcesHandler;
    }

    @GET
    @Produces("text/xml")
    @Path("/value/{id}")
    public GetValueResponse getValue(@PathParam("id") int id) {

        try {

            GetValueResponse resp = new GetValueResponse();
            resp.setId(id);
            resp.setValue(resourcesHandler.getValue(id));

            return resp;

        } catch (Throwable t) {
            logger.error("error processing GET", t);
            throw new RuntimeException(t);
        }

    }

    @POST
    @Produces("text/xml")
    @Path("/value/{id}/conversion/{conversionValueId}/delta/{deltaAmount}")
    public PostValueResponse updateValue(
            @PathParam("id") int id,
            @PathParam("conversionValueId") int conversionValueId,
            @PathParam("deltaAmount") BigDecimal deltaAmount) {

        try {

            resourcesHandler.update(id, conversionValueId, deltaAmount);
            return PostValueResponse.operationSucceeded();

        } catch (Throwable t) {
            logger.error("error processing POST", t);
            return PostValueResponse.operationFailed();
        }
    }

}