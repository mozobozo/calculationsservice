package ua.com.newground.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.com.newground.services.resources.IndexedResource;

import java.math.BigDecimal;

import static java.math.BigDecimal.TEN;

/**
 * Handles the logic of reading and updating values
 */
public class ResourcesHandler {
    private static Logger logger = LoggerFactory.getLogger(ResourcesHandler.class);

    private final IndexedResource<BigDecimal> inputConversionResource;
    private final IndexedResource<BigDecimal> intermediateResultsResource;

    public ResourcesHandler(IndexedResource<BigDecimal> inputConversionResource, IndexedResource<BigDecimal> intermediateResultsResource) {
        this.inputConversionResource = inputConversionResource;
        this.intermediateResultsResource = intermediateResultsResource;
    }

    public void update(int valueId, int conversionValueId, BigDecimal deltaAmount) {
        BigDecimal conversionValue = inputConversionResource.get(conversionValueId);

        logger.debug("using conversion value {} and delta {}", conversionValue, deltaAmount);
        BigDecimal convertedValue = conversionValue.add(deltaAmount);

        if (convertedValue.compareTo(TEN) < 0) {
            logger.debug("applying +10 correction");
            convertedValue = convertedValue.add(TEN);
        }

        logger.debug("saving values[{}] = {}", valueId, convertedValue);
        intermediateResultsResource.put(valueId, convertedValue);
    }

    public BigDecimal getValue(int valueId) {
        BigDecimal currentValue = intermediateResultsResource.get(valueId);

        if (currentValue.compareTo(TEN) > 0) {
            logger.debug("applying -10 correction for {}", currentValue);
            return  currentValue.subtract(TEN);
        }
        return currentValue;
    }
}