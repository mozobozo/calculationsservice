package ua.com.newground.services.responses;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "operationResultCode")
public class PostValueResponse {

    enum ResultCode {

        OPERATION_SUCCEEDED(1),
        OPERATION_FAILED(0);

        private int value;

        private ResultCode(int value) {
            this.value = value;
        }
    }

    private int code;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public static PostValueResponse operationSucceeded() {
        PostValueResponse response = new PostValueResponse();
        response.code = ResultCode.OPERATION_SUCCEEDED.value;
        return response;
    }

    public static PostValueResponse operationFailed() {
        PostValueResponse response = new PostValueResponse();
        response.code = ResultCode.OPERATION_FAILED.value;
        return response;
    }

}
