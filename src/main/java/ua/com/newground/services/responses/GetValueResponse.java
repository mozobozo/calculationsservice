package ua.com.newground.services.responses;

import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;

@XmlRootElement(name = "result")
public class GetValueResponse {

    private int id;
    private BigDecimal value;

    public int getId() {
        return id;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
