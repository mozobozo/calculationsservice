package ua.com.newground;

import org.mortbay.jetty.Server;
import org.mortbay.jetty.webapp.WebAppContext;

/**
 * Helper class to be used in manual and automated tests.
 * Starts embedded jersey server with CalculationsServer and its spring context.
 */
public class EmbeddedJerseyTestServer {

    private Server server;

    public EmbeddedJerseyTestServer() {
        WebAppContext webAppContext = new WebAppContext();
        webAppContext.setWar("src/main/resources/");
        webAppContext.setContextPath("/");


        server = new Server(8080);
        server.setHandler(webAppContext);
    }

    public void start() throws Exception {
        server.start();
    }

    public void stop() throws Exception {
        server.stop();
    }

    public static void main(String[] args) throws Exception {
        EmbeddedJerseyTestServer server = new EmbeddedJerseyTestServer();
        server.start();
        System.in.read();
        server.stop();
    }
}
