package ua.com.newground.services;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import ua.com.newground.EmbeddedJerseyTestServer;

import java.io.File;

import static com.jayway.restassured.RestAssured.expect;
import static org.apache.commons.io.FileUtils.writeStringToFile;
import static org.hamcrest.Matchers.equalTo;

public class CalculationsServiceTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    private String conversionsFilePath;
    private String intermediateValuesPath;

    private EmbeddedJerseyTestServer server;

    @Before
    public void setupTest() throws Exception {
        String rootPath = temporaryFolder.getRoot().getAbsolutePath();
        conversionsFilePath = rootPath + File.separator + "conversions.csv";
        intermediateValuesPath = rootPath + File.separator + "intermediate-values.csv";

        System.setProperty("csv.files.dir", rootPath + File.separator);
        System.setProperty("conversions.file.path",  "conversions.csv" );
        System.setProperty("intermediate.vals.file.path", "intermediate-values.csv");
    }

    @Test
    public void shouldReturnCorrectValuesOnGet() throws Exception {

        writeStringToFile(new File(intermediateValuesPath), "0.5,10");
        server = new EmbeddedJerseyTestServer();
        server.start();

        // get value with id = 0
        expect().body(
                    "result.value", equalTo("0.5"),
                    "result.id",    equalTo("0")).
        when().request()
                    .get("/calculations/value/0");


        // get value with id = 1
        expect().body(
                    "result.value", equalTo("10"),
                    "result.id",    equalTo("1")).
        when().request()
                    .get("/calculations/value/1");
    }

    @Test
    public void shouldUpdateValues() throws Exception {
        writeStringToFile(new File(conversionsFilePath), "1,2");
        writeStringToFile(new File(intermediateValuesPath), "0");
        server = new EmbeddedJerseyTestServer();
        server.start();

        /*
         post url looks like
         /value/{id}/conversion/{conversionValueId}/delta/{deltaAmount}
        */
        expect().body("operationResultCode.code", equalTo("1")).when().request().post("/calculations/value/0/conversion/1/delta/-3.00125");
        expect().body("operationResultCode.code", equalTo("1")).when().request().post("/calculations/value/5/conversion/0/delta/-0");

        expect().body("result.value", equalTo("8.99875")).when().get("/calculations/value/0"); // + 10 rule applied too
        expect().body("result.value", equalTo("1")).when().get("/calculations/value/5");
    }

    @Test
    public void shouldReturnFailedResponseOnFailedUpdates() throws Exception {
        EmbeddedJerseyTestServer server = new EmbeddedJerseyTestServer();
        server.start();

        // negative ids in url lead to failed code = 0
        expect().body("operationResultCode.code", equalTo("0")).when().request().post("/calculations/value/-1/conversion/-1/delta/-0");
    }

    @After
    public void stopServerIfStarted() throws Exception {
        if (server != null) {
            server.stop();
            server = null;
        }
    }
}
