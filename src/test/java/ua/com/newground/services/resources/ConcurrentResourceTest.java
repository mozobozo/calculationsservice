package ua.com.newground.services.resources;

import org.junit.Test;

import java.util.concurrent.CountDownLatch;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class ConcurrentResourceTest {

    @Test(timeout = 200)
    /**
     * Checks that resource can be accessed for read concurrently
     * each read takes 100ms so test passes if 10 reads complete in less than 200
     */
    public void concurrentReadsShouldNotBlockEachOther() throws InterruptedException {
        final ConcurrentResource<Integer> resource = new ConcurrentResource<Integer>(new SlowSingleValueResource(100, 0));

        int threadsNumber = 10;

        final CountDownLatch startGate = new CountDownLatch(1);
        final CountDownLatch endGate = new CountDownLatch(threadsNumber);

        Runnable readValueTask = new Runnable() {
            @Override
            public void run() {
                try {
                    startGate.await();
                    resource.get(0);
                    endGate.countDown();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        
        for(int i = 0; i < threadsNumber; i++) {
            Thread thread = new Thread(readValueTask);
            thread.start();
        }

        startGate.countDown();
        endGate.await();
    }

    @Test
    public void checkConcurrentReadsAndWrites() throws InterruptedException {
        final ConcurrentResource<Integer> resource = new ConcurrentResource<Integer>(new SlowSingleValueResource(0, 1000));

        assertThat(resource.get(0), is(0));

        new Thread(new Runnable() {
            @Override
            public void run() {
                resource.put(0,-5);
            }
        }).start(); // takes 1000 ms to finish

        Thread.sleep(10);
        assertThat(resource.get(0), is(-5)); // -5 means it waited for update to finish
    }


    private class SlowSingleValueResource implements IndexedResource<Integer> {

        private final long readBlockTime;
        private final long writeBlockTime;
        private volatile int currentValue = 0;

        private SlowSingleValueResource(long readBlockTime, long writeBlockTIme) {
            this.readBlockTime = readBlockTime;
            this.writeBlockTime = writeBlockTIme;
        }

        @Override
        public Integer get(int id) {
            block(readBlockTime);
            return currentValue;
        }

        @Override
        public void put(int id, Integer value) {
            block(writeBlockTime);
            currentValue = value;
        }

        private void block(long sleepTime) {
            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
