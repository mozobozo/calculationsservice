package ua.com.newground.services.resources;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;

import static java.math.BigDecimal.*;
import static org.apache.commons.io.FileUtils.readFileToString;
import static org.apache.commons.io.FileUtils.writeStringToFile;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;


public class CsvFileResourceTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Test
    public void shouldReadValuesFromCsvFile() throws IOException {
        File csvFile = createCsvResourceFile("1,2.00002,-0,-4.1,0");
        CsvFileResource csvFileResource = new CsvFileResource(csvFile.getAbsolutePath());

        assertThat(csvFileResource.get(0), is(ONE));
        assertThat(csvFileResource.get(1), is(new BigDecimal("2.00002")));
        assertThat(csvFileResource.get(2), is(ZERO));
        assertThat(csvFileResource.get(3), is(new BigDecimal("-4.1")));
        assertThat(csvFileResource.get(4), is(ZERO));
    }

    @Test
    public void shouldReadFileWithMissingValues() throws IOException {
        File csvFile = createCsvResourceFile("0,,10");
        CsvFileResource csvFileResource = new CsvFileResource(csvFile.getAbsolutePath());

        assertThat(csvFileResource.get(0), is(ZERO));
        assertThat(csvFileResource.get(2), is(TEN));
    }

    @Test
    public void shouldWriteValuesToFile() throws IOException {
        File csvFile = createCsvResourceFile("0,1");
        CsvFileResource csvFileResource = new CsvFileResource(csvFile.getAbsolutePath());

        assertThat(csvFileResource.get(0), is(ZERO));

        BigDecimal newValue = new BigDecimal("-0.000002");
        csvFileResource.put(0, newValue);

        // check that resource value was updated
        assertThat(csvFileResource.get(0), is(newValue));

        // check that underlying file content was updated
        String content = readFileToString(csvFile).trim();
        assertThat(content, is("-0.000002,1"));
    }

    @Test
    public void shouldBeAbleToAddNewValues() throws IOException {
        File csvFile = createCsvResourceFile("0,1");
        CsvFileResource csvFileResource = new CsvFileResource(csvFile.getAbsolutePath());

        csvFileResource.put(5, TEN);
        assertThat(csvFileResource.get(5), is(TEN));

        String content = readFileToString(csvFile).trim();
        assertThat(content, is("0,1,,,,10"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowAnExceptionWhenReadingEmptyValue() throws IOException {
        File csvFile = createCsvResourceFile("0,,1");
        CsvFileResource csvFileResource = new CsvFileResource(csvFile.getAbsolutePath());

        csvFileResource.get(1); // throws exception
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowAnExceptionWhenReadingValueOutOfBounds() throws IOException {
        File csvFile = createCsvResourceFile("0,,1");
        CsvFileResource csvFileResource = new CsvFileResource(csvFile.getAbsolutePath());

        csvFileResource.get(100); // throws exception
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowAnExceptionWhenUpdatingNegativeValue() throws IOException {
        File csvFile = createCsvResourceFile("0,1,2");
        CsvFileResource csvFileResource = new CsvFileResource(csvFile.getAbsolutePath());

        csvFileResource.put(-1, TEN); // throws exception
    }

    private File createCsvResourceFile(String content) throws IOException {
        File csvFile = temporaryFolder.newFile("test.csv");
        writeStringToFile(csvFile, content);
        return csvFile;
    }
}
