package ua.com.newground.services;

import org.junit.Test;
import ua.com.newground.services.resources.IndexedResource;

import static java.math.BigDecimal.valueOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ResourcesHandlerTest {

    @Test
    public void testGetCalculationsLogic() {
        IndexedResource mockResource = mock(IndexedResource.class);
        ResourcesHandler handler = new ResourcesHandler(null, mockResource);

        // less than ten
        when(mockResource.get(0)).thenReturn(valueOf(5));
        assertThat(handler.getValue(0), is(valueOf(5)));

        // greater than ten
        when(mockResource.get(1)).thenReturn(valueOf(100));
        assertThat(handler.getValue(1), is(valueOf(90)));
    }

    @Test
    public void testPostMethodLogic() {
        IndexedResource inputConversionResource = mock(IndexedResource.class);
        IndexedResource mockResource = mock(IndexedResource.class);

        ResourcesHandler handler = new ResourcesHandler(inputConversionResource, mockResource);

        // less than ten
        when(inputConversionResource.get(0)).thenReturn(valueOf(9));
        handler.update(1, 0, valueOf(0.9));
        verify(mockResource).put(1, valueOf(19.9));

        // greater than ten
        when(inputConversionResource.get(1)).thenReturn(valueOf(100));
        handler.update(2, 1, valueOf(-20.5));
        verify(mockResource).put(2, valueOf(79.5));
    }

}
